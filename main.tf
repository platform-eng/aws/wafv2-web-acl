module "wafv2-web-acl" {
  source   = "umotif-public/waf-webaclv2/aws"
  version  = "4.6.1"
  for_each = var.webacl_config
  name_prefix                  = each.key
  alb_arn                      = try(each.value.alb_arn, var.alb_arn)
  alb_arn_list                 = try(each.value.alb_arn_list, var.alb_arn_list)
  tags                         = try(each.value.tags, var.tags)
  rules                        = try(each.value.rules, var.rules)
  visibility_config            = try(each.value.visibility_config, var.visibility_config)
  create_alb_association       = try(each.value.create_alb_association, var.create_alb_association)
  create_logging_configuration = try(each.value.create_logging_configuration, var.create_logging_configuration)
  log_destination_configs      = try(each.value.log_destination_configs, var.log_destination_configs)
  redacted_fields              = try(each.value.redacted_fields, var.redacted_fields)
  allow_default_action         = try(each.value.allow_default_action, var.allow_default_action)
  scope                        = try(each.value.scope, var.scope)
  logging_filter               = try(each.value.logging_filter, var.logging_filter)
  description                  = try(each.value.description, var.description)
  custom_response_bodies       = try(each.value.custom_response_bodies, var.custom_response_bodies)
}
