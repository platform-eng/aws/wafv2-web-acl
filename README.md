# wafv2-web-acl

Terraform module which creates WAFV2 Web ACLs' on AWS with all (or almost all) features provided by Terraform AWS provider using YAML file.

## Usage

You can use this module to provision Web ACLs by providing configuration through YAML files provided YAML file is written in specified syntax.

### &nbsp;Steps to follow for temporary resource provision from local device :

_Prequisites : You need to install  [` aws cli ` ](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)  and [`Terraform`](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) on your local device and configure `aws` account keys._

1.  &nbsp;Clone the git repository and go inside folder of repositroy. <br>
2.  &nbsp;Create a yaml file for input to module. Follow below syntax to write yaml file.<br>
3.  &nbsp;Run command to convert yaml to "terraform.tfvars.json" :
    <br>
    ```shell
     yq <yaml_filename> -o=json > terraform.tfvars.json
    ```
    &nbsp;&nbsp;If `yq` is not installed please refer <a href="https://github.com/mikefarah/yq">here</a> . <br>
4.  &nbsp;After that initialize [`terraform `](https://developer.hashicorp.com/terraform/cli/commands/init). <br>
    **If you want your state file to get stored on s3** &nbsp; then run <br>

    ```shell
    terraform init --backend=true -backend-config=bucket=<BACKEND_CONFIG_BUCKET> -backend-config=key=<BACKEND_CONFIG_KEY>
    ```

    **_BACKEND_CONFIG_BUCKET_** : bucket name where you want tfstate to be stored <br>
    **_BACKEND_CONFIG_KEY_** : file name inside bucket.<br><br>
    &nbsp;&nbsp;**Note**: &nbsp;you will have to give name of an existing bucket to store statefile.<br>&nbsp;&nbsp;&nbsp;&nbsp;(statefile name can be given as "terraform.tfstate")
    <br>
    **If you want state file to get stored on local** &nbsp; then after git clone you will get `backend.tf` delete that file and then run `terraform init`

5.  &nbsp;Then run command `terraform plan` to check the resources that terraform will create. <br>
6.  &nbsp;Run command `terraform apply` to create resources. <br><br>

### &nbsp;Steps to follow if you want to store files inside gitlab and execute pipeline
1. Create your private repo in gitlab. <br>
2. Create your yaml file e.g. `dev.terraform.tfvars.yaml`. Can change the name of the file from `dev` to `labs/qa/staging/pt/prod/non-prod`.You can choose file name according to the workspaces or environment. <br>
  &nbsp; **NOTE** : Name of the YAML file `must` be from `labs/dev/qa/staging/pt/prod/non-prod` these only.<br>
3. Also create `.gitlab-ci.yml` .
  - If you want to use our generic template then you need to include the path `platform-eng/gitlab-ci-templates/public/platform` and filename `generic-deployment.yaml`
  - Need to give values for the variables <br>`NON_PROD_BACKEND_CONFIG_BUCKET`- bucketname for non-prod backend config and to store tfstate file<br>`PROD_BACKEND_CONFIG_BUCKET`-bucketname for prod backend config and to store tfstate file<br>`BACKEND_CONFIG_KEY`-filename to store the configuration<br>`PROVISIONER_NAME`-name of the resource must be exctly the same which we have given to the resource<br>`PROVISIONER_URL`-https url of gitlab resource which will clone our terraform code and execute your `yaml` and `gitlab-ci files`.<br>
  **NOTE: If you want to use your own runner instead of the shared, you will have to provide the variable `RUNNER_TAG` with your runner tag.
  It is optional if you do not provide `RUNNER_TAG` it will run using the shared runners.**
  
    ### EXAMPLE:
    #### `.gitlab-ci.yml`
    ```yaml
    include:
    - project: 'platform-eng/gitlab-ci-templates/public/platform'
      file: 'generic-deployment.yaml'

    variables:
      NON_PROD_BACKEND_CONFIG_BUCKET: abc-state-files
      PROD_BACKEND_CONFIG_BUCKET: abc-state-files
      BACKEND_CONFIG_KEY: test-1
      PROVISIONER_NAME: rds
      PROVISIONER_URL: https://gitlab.com/platform-eng/aws/rds.git
      RUNNER_TAG: test
    ```
***You will get all our resources [here](https://gitlab.com/platform-eng/aws)
and our blueprints [here](https://gitlab.com/platform-eng/gitlab-ci-templates/public/platform)***


<br>

## SYNTAX

&nbsp; Input YAML file should consist of element "webacl_config" to create Web ACLs. 
<br>
&nbsp; &nbsp;&nbsp; An element "common_config" is required in YAML.

```yaml

common_config:
  region: <region_code>
  allowed_account_ids:
    - <account_id1>
    - <account_id2>
  default_tags:
    <tag1>: <value1>
    <tag2>: <value2>

```

## Examples

<br>

### Web ACL associating with ALB with examples of all rules : 


 ```yaml

 common_config:
  region: us-east-1
  allowed_account_ids:
    - 123456789
  default_tags:
    env: test
    project: labs

 webacl_config:
  test-waf-setup:
    alb_arn: module.alb.arn
    scope: "REGIONAL"
    create_alb_association: true
    allow_default_action: true # set to allow if not specified
    visibility_config:
      metric_name: "test-waf-setup-waf-main-metrics"
    rules:
        #Managed Rules
      - name: "AWSManagedRulesCommonRuleSet-rule-1"
        priority: "1"
        override_action: "none"
        visibility_config:
          metric_name: "AWSManagedRulesCommonRuleSet-metric"
        managed_rule_group_statement:
          name: "AWSManagedRulesCommonRuleSet"
          vendor_name: "AWS"
          rule_action_overrides:
            - name: "SizeRestrictions_QUERYSTRING"
              action_to_use:
                count: {}
            - name: "SizeRestrictions_BODY"
              action_to_use:
                count: {}
            - action_to_use:
                count: {}
              name: "GenericRFI_QUERYARGUMENTS"
      - name: "AWSManagedRulesKnownBadInputsRuleSet-rule-2"
        priority: "2"
        override_action: "count"
        visibility_config:
          metric_name: "AWSManagedRulesKnownBadInputsRuleSet-metric"
        managed_rule_group_statement:
          - name: "AWSManagedRulesKnownBadInputsRuleSet"
            vendor_name: "AWS"
      - name: "AWSManagedRulesPHPRuleSet-rule-3"
        priority: "3"
        override_action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "AWSManagedRulesPHPRuleSet-metric"
          sampled_requests_enabled: false
        managed_rule_group_statement:
          - name: "AWSManagedRulesPHPRuleSet"
            vendor_name: "AWS"
      ### Byte Match Rule example
      # Refer to https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl#byte-match-statement
      # for all of the options available.
      # Additional examples available in the examples directory
      - name: "ByteMatchRule-4"
        priority: "4"
        action: "count"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "ByteMatchRule-metric"
          sampled_requests_enabled: false
        byte_match_statement:
          field_to_match:
            uri_path: "{}"
          positional_constraint: "STARTS_WITH"
          search_string: "/path/to/match"
          priority: 0
          type: "NONE"
      ### Geo Match Rule example
      - name: "GeoMatchRule-5"
        priority: "5"
        action: "allow"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "GeoMatchRule-metric"
          sampled_requests_enabled: false
        geo_match_statement:
          country_codes:
            - "NL"
            - "GB"
            - "US"
      ### IP Set Rule example
      - name: "IpSetRule-6"
        priority: "6"
        action: "allow"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "IpSetRule-metric"
          sampled_requests_enabled: false
        ip_set_reference_statement:
          arn: "arn:aws:wafv2:eu-west-1:111122223333:regional/ipset/ip-set-test/a1bcdef2-1234-123a-abc0-1234a5bc67d8"
      ### IP Rate Based Rule example
      - name: "IpRateBasedRule-7"
        priority: "7"
        action: "block"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "IpRateBasedRule-metric"
          sampled_requests_enabled: false
        rate_based_statement:
          limit: 100
          aggregate_key_type: "IP"
          # Optional scope_down_statement to refine what gets rate limited
          scope_down_statement:
            not_statement:
              byte_match_statement:
                field_to_match:
                  uri_path: "{}"
                positional_constraint: "STARTS_WITH"
                search_string: "/path/to/match"
                priority: 0
                type: "NONE"
      ### NOT rule example (can be applied to byte_match, geo_match, and ip_set rules)
      - name: "NotByteMatchRule-8"
        priority: "8"
        action: "count"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "NotByteMatchRule-metric"
          sampled_requests_enabled: false
        not_statement:
          byte_match_statement:
            field_to_match:
              uri_path: "{}"
            positional_constraint: "STARTS_WITH"
            search_string: "/path/to/match"
            priority: 0
            type: "NONE"
      ### Regex Match Rule example
      - name: "RegexMatchRule-9"
        priority: "9"
        action: "allow"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "RegexMatchRule-metric"
          sampled_requests_enabled: false
        byte_match_statement:
          field_to_match:
            uri_path: "{}"
          regex_string: "/foo/"
          priority: 0
          type: "NONE"
      ### Attach Custom Rule Group example
      - name: "CustomRuleGroup-1"
        priority: "9"
        override_action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "CustomRuleGroup-metric"
          sampled_requests_enabled: false
        rule_group_reference_statement:
          arn: "arn:aws:wafv2:eu-west-1:111122223333:regional/rulegroup/rulegroup-test/a1bcdef2-1234-123a-abc0-1234a5bc67d8"
      ### Size constraint Rule example
      # Refer to https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl#size-constraint-statement
      # for all of the options available.
      # Additional examples available in the examples directory
      - name: "BodySizeConstraint"
        priority: 0
        size_constraint_statement:
          field_to_match:
            body: "{}"
          comparison_operator: "GT"
          size: 8192
          priority: 0
          type: "NONE"
        action: "count"
        visibility_config:
          cloudwatch_metrics_enabled: true
          metric_name: "BodySizeConstraint"
          sampled_requests_enabled: true
      ### Regex Pattern Set Reference Rule example
      # Refer to https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl#regex-pattern-set-reference-statement
      # for all of the options available.
      # Additional examples available in the examples directory
      - name: "MatchRegexRule-1"
        priority: "1"
        action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: true
          metric_name: "RegexBadBotsUserAgent-metric"
          sampled_requests_enabled: false
        # You need to previously create you regex pattern
        # Refer to https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_regex_pattern_set
        # for all of the options available.
        regex_pattern_set_reference_statement:
          arn: aws_wafv2_regex_pattern_set.example.arn
          field_to_match:
            single_header:
              name: "user-agent"
          priority: 0
          type: "LOWERCASE" # The text transformation type
    tags:
      Name: "test-waf-setup"
      Env: "test"

```
<br>

### Web ACL with Logging Configuration Enabled 

```yaml
common_config:
  region: us-east-1
  allowed_account_ids:
    - 123456789
  default_tags:
    env: test
    project: labs

webacl_config:
  test-waf-setup:
    name_prefix: "test-waf-setup"
    alb_arn: <alb_arn>
    create_alb_association: true
    create_logging_configuration: true
    log_destination_configs:
      - <Cloudwatch LogGroup/Amazon Kinesis Data Firehose/S3 bucket ARN>
    redacted_fields:
      - single_header:
          name: "user-agent"
    logging_filter:
      default_behavior: "DROP"
      filter:
        - behavior: "KEEP"
          requirement: "MEETS_ANY"
          condition:
            - action_condition:
                action: "ALLOW"
        - behavior: "DROP"
          requirement: "MEETS_ALL"
          condition:
            - action_condition:
                action: "COUNT"
              label_name_condition:
                label_name: "awswaf:111122223333:rulegroup:testRules:LabelNameZ"
    visibility_config:
      cloudwatch_metrics_enabled: false
      metric_name: "test-waf-setup-waf-main-metrics"
      sampled_requests_enabled: false
    rules:
      - name: "AWSManagedRulesCommonRuleSet-rule-1"
        priority: "1"
        override_action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "AWSManagedRulesCommonRuleSet-metric"
          sampled_requests_enabled: false
        managed_rule_group_statement:
          name: "AWSManagedRulesCommonRuleSet"
          vendor_name: "AWS"
      - name: "AWSManagedRulesKnownBadInputsRuleSet-rule-2"
        priority: "2"
        override_action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "AWSManagedRulesKnownBadInputsRuleSet-metric"
          sampled_requests_enabled: false
        managed_rule_group_statement:
          name: "AWSManagedRulesKnownBadInputsRuleSet"
          vendor_name: "AWS"
      - name: "AWSManagedRulesPHPRuleSet-rule-3"
        priority: "3"
        override_action: "none"
        visibility_config:
          cloudwatch_metrics_enabled: false
          metric_name: "AWSManagedRulesPHPRuleSet-metric"
          sampled_requests_enabled: false
        managed_rule_group_statement:
          name: "AWSManagedRulesBotControlRuleSet"
          vendor_name: "AWS"
    tags:
      Environment: "test"

```
<br>

## Inputs

<br>

### &nbsp;&nbsp;With each key of element "webacl_config" following inputs can be provided :

| Name                                                                                                                  | Description                                                                                                                                                                                                                                 | Type                                                                                             | Default      | Required |
| --------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ------------ | :------: |
| <a name="input_alb_arn"></a> [alb_arn](#input_alb_arn)                                                                | Application Load Balancer ARN                                                                                                                                                                                                               | `string`                                                                                         | `""`         |    no    |
| <a name="input_alb_arn_list"></a> [alb_arn_list](#input_alb_arn_list)                                                 | Application Load Balancer ARN list                                                                                                                                                                                                          | `list(string)`                                                                                   | `[]`         |    no    |
| <a name="input_allow_default_action"></a> [allow_default_action](#input_allow_default_action)                         | Set to `true` for WAF to allow requests by default. Set to `false` for WAF to block requests by default.                                                                                                                                    | `bool`                                                                                           | `true`       |    no    |
| <a name="input_create_alb_association"></a> [create_alb_association](#input_create_alb_association)                   | Whether to create alb association with WAF web acl                                                                                                                                                                                          | `bool`                                                                                           | `true`       |    no    |
| <a name="input_create_logging_configuration"></a> [create_logging_configuration](#input_create_logging_configuration) | Whether to create logging configuration in order start logging from a WAFv2 Web ACL to Amazon Kinesis Data Firehose.                                                                                                                        | `bool`                                                                                           | `false`      |    no    |
| <a name="input_custom_response_bodies"></a> [custom_response_bodies](#input_custom_response_bodies)                   | Custom response bodies to be referenced on a per rule basis. https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_web_acl#custom-response-body                                                                 | <pre>list(object({<br> key = string<br> content = string<br> content_type = string<br> }))</pre> | `[]`         |    no    |
| <a name="input_description"></a> [description](#input_description)                                                    | A friendly description of the WebACL                                                                                                                                                                                                        | `string`                                                                                         | `null`       |    no    |
| <a name="input_enabled"></a> [enabled](#input_enabled)                                                                | Whether to create the resources. Set to `false` to prevent the module from creating any resources                                                                                                                                           | `bool`                                                                                           | `true`       |    no    |
| <a name="input_log_destination_configs"></a> [log_destination_configs](#input_log_destination_configs)                | The Amazon Kinesis Data Firehose Amazon Resource Name (ARNs) that you want to associate with the web ACL. Currently, only 1 ARN is supported.                                                                                               | `list(string)`                                                                                   | `[]`         |    no    |
| <a name="input_logging_filter"></a> [logging_filter](#input_logging_filter)                                           | A configuration block that specifies which web requests are kept in the logs and which are dropped. You can filter on the rule action and on the web request labels that were applied by matching rules during web ACL evaluation.          | `any`                                                                                            | `{}`         |    no    |
| <a name="input_name_prefix"></a> [name_prefix](#input_name_prefix)                                                    | Name prefix used to create resources.                                                                                                                                                                                                       | `string`                                                                                         | n/a          |   yes    |
| <a name="input_redacted_fields"></a> [redacted_fields](#input_redacted_fields)                                        | The parts of the request that you want to keep out of the logs. Up to 100 `redacted_fields` blocks are supported.                                                                                                                           | `any`                                                                                            | `[]`         |    no    |
| <a name="input_rules"></a> [rules](#input_rules)                                                                      | List of WAF rules.                                                                                                                                                                                                                          | `any`                                                                                            | `[]`         |    no    |
| <a name="input_scope"></a> [scope](#input_scope)                                                                      | Specifies whether this is for an AWS CloudFront distribution or for a regional application. Valid values are CLOUDFRONT or REGIONAL. To work with CloudFront, you must also specify the region us-east-1 (N. Virginia) on the AWS provider. | `string`                                                                                         | `"REGIONAL"` |    no    |
| <a name="input_tags"></a> [tags](#input_tags)                                                                         | A map of tags (key-value pairs) passed to resources.                                                                                                                                                                                        | `map(string)`                                                                                    | `{}`         |    no    |
| <a name="input_visibility_config"></a> [visibility_config](#input_visibility_config)                                  | Visibility config for WAFv2 web acl. https://www.terraform.io/docs/providers/aws/r/wafv2_web_acl.html#visibility-configuration                                                                                                              | `map(string)`                                                                                    | `{}`         |    no    |

---
